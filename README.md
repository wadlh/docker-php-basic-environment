
## 目录结构：
- app：项目运行目录
- bin：Dockerfile目录
- data：数据存放目录
- etc：配置文件目录
- log：日志文件目录

## 注意事项：
- 通过nginx代理运行项目请配置 etc/nginx/conf.d/default.conf,参考default.conf-example

- 如需运行守护进程监听，启动supervisor：/usr/bin/supervisord -c /etc/supervisor/supervisord.conf

- 容器内若无法composer下载包，运行：composer config -g repo.packagist composer https://packagist.org